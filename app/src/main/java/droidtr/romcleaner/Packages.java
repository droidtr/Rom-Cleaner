package droidtr.romcleaner;

import android.content.*;
import android.content.pm.*;
import java.util.*;

public class Packages {
	private PackageManager pm;
	private List<PackageInfo> pi;

	Packages(Context c){
		pm = c.getPackageManager();
		pi = pm.getInstalledPackages(PackageManager.GET_META_DATA);
	}

	public PackageInfo getPackage(int index){
		return pi.get(index);
	}

	public int getCount(){
		return pi.size();
	}

	public String getPackageName(int index){
		return getPackage(index).packageName;
	}

	public String[] getAllPackageNames(){
		StringBuilder sb = new StringBuilder();
		for(int i = 0;i != getCount();i++)
			sb.append(getPackageName(i)+";");
		return sb.toString().split(";");
	}

	public String getAppName(int index){
		return getPackage(index).applicationInfo.loadLabel(pm).toString();
	}

	public String[] getAllAppNames(){
		StringBuilder sb = new StringBuilder();
		for(int i = 0;i != getCount();i++)
			sb.append(getAppName(i)+";");
		return sb.toString().split(";");
	}

	public String getPackageLocation(int index){
		return getPackage(index).applicationInfo.sourceDir;
	}

	public String[] getAllPackageLocations(){
		StringBuilder sb = new StringBuilder();
		for(int i = 0;i != getCount();i++)
			sb.append(getPackageLocation(i)+";");
		return sb.toString().split(";");
	}

	public String[] getAllSystemAppsPackageLocations(){
		StringBuilder sb = new StringBuilder();
		for(String s : getAllPackageLocations())
			if(!s.startsWith("/data"))
				sb.append(s+";");
		return sb.toString().split(";");
	}

}
