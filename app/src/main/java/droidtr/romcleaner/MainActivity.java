package droidtr.romcleaner;

import android.app.*;
import android.graphics.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import java.io.*;

import java.lang.Process;

public class MainActivity extends Activity {
    String[] gapps1 = new String[]{"/system/app/Books", "/system/app/CalculatorGoogle", "/system/app/CalendarGooglePrebuilt", "/system/app/Chrome", "/system/app/CloudPrint2", "/system/app/DMAgent", "/system/app/Drive", "/system/app/EditorsDocs", "/system/app/EditorsSheets", "/system/app/EditorsSlides", "/system/app/FitnessPrebuilt", "/system/priv-app/GmsCore","/system/app/GoogleCalendarSyncAdapter", "/system/app/GoogleCamera", "/system/app/GoogleCamera", "/system/app/GoogleContactsSyncAdapter", "/system/app/GoogleEars", "/system/app/GoogleEarth", "/system/app/GoogleHindiIME", "/system/app/GoogleHome", "/system/app/GoogleJapaneseInput", "/system/app/GooglePinyinIME", "/system/app/GoogleTTS", "/system/app/GoogleZhuyinIME", "/system/app/Hangouts", "/system/app/KoreanIME", "/system/app/LatinImeGoogle", "/system/app/Maps", "/system/app/Music2", "/system/app/Newsstand", "/system/app/Photos", "/system/app/PlayGames", "/system/app/PlusOne", "/system/app/PrebuiltBugle", "/system/app/PrebuiltDeskClockGoogle", "/system/app/PrebuiltExchange3Google", "/system/app/PrebuiltGmail", "/system/app/PrebuiltKeep", "/system/app/PrebuiltNewsWeather", "/system/app/Street", "/system/app/TranslatePrebuilt", "/system/app/Tycho", "/system/app/Videos", "/system/app/Wallet", "/system/app/WebViewGoogle", "/system/app/YouTube", "/system/app/talkback", "/system/etc/permissions/com.google.android.camera.experimental2015.xml", "/system/etc/permissions/com.google.android.camera2.xml", "/system/etc/permissions/com.google.android.dialer.support.xml", "/system/etc/permissions/com.google.android.maps.xml", "/system/etc/permissions/com.google.android.media.effects.xml", "/system/etc/permissions/com.google.widevine.software.drm.xml", "/system/etc/preferred-apps/google.xml", "/system/etc/sysconfig/google.xml", "/system/etc/sysconfig/google_build.xml", "/system/etc/sysconfig/whitelist_com.android.omadm.service.xml", "/system/framework/com.google.android.camera.experimental2015.jar", "/system/framework/com.google.android.camera2.jar", "/system/framework/com.google.android.dialer.support.jar", "/system/framework/com.google.android.maps.jar", "/system/framework/com.google.android.media.effects.jar", "/system/framework/com.google.widevine.software.drm.jar", "/system/lib/libfilterpack_facedetect.so", "/system/lib/libjni_keyboarddecoder.so", "/system/lib/libjni_latinimegoogle.so", "/system/priv-app/ConfigUpdater", "/system/priv-app/GCS", "/system/priv-app/GoogleBackupTransport", "/system/priv-app/GoogleContacts", "/system/priv-app/GoogleDialer", "/system/priv-app/GoogleFeedback", "/system/priv-app/GoogleLoginService", "/system/priv-app/GoogleOneTimeInitializer", "/system/priv-app/GooglePackageInstaller", "/system/priv-app/GooglePartnerSetup", "/system/priv-app/GoogleServicesFramework", "/system/priv-app/Phonesky", "/system/priv-app/PrebuiltGmsCore", "/system/priv-app/SetupWizard", "/system/priv-app/TagGoogle", "/system/priv-app/Velvet", "/system/usr/srec/en-US/", "/system/tts","/system/app/GoogleWallpaperPicker","/system/app/VPapers/","/system/app/GDeskClock/"};
    String[] gapps2 = new String[]{"/system/app/ChromeWithBrowser.apk", "/system/app/GMS_Maps.apk", "/system/app/GmsCore.apk", "/system/app/Books.apk", "/system/app/CalendarGoogle.apk", "/system/app/Chrome.apk", "/system/app/ChromeBookmarksSyncAdapter.apk", "/system/app/CloudPrint2.apk", "/system/app/Drive.apk", "/system/app/EmailGoogle.apk", "/system/app/Exchange2Google.apk", "/system/app/FaceLock.apk", "/system/app/GalleryGoogle.apk", "/system/app/GenieWidget.apk", "/system/app/Gmail2.apk", "/system/app/GoogleCalendarSyncAdapter.apk", "/system/app/GoogleCamera.apk", "/system/app/GoogleContactsSyncAdapter.apk", "/system/app/GoogleEars.apk", "/system/app/GoogleEarth.apk", "/system/app/GoogleHome.apk", "/system/app/GoogleTTS.apk", "/system/app/Hangouts.apk", "/system/app/Keep.apk", "/system/app/LatinImeGoogle.apk", "/system/app/Magazines.apk", "/system/app/Maps.apk", "/system/app/Music2.apk", "/system/app/PlayGames.apk", "/system/app/PlusOne.apk", "/system/app/QuickOffice.apk", "/system/app/Street.apk", "/system/app/SunBeam.apk", "/system/app/Videos.apk", "/system/app/YouTube.apk", "/system/etc/permissions/com.google.android.ble.xml", "/system/etc/permissions/com.google.android.camera2.xml", "/system/etc/permissions/com.google.android.maps.xml", "/system/etc/permissions/com.google.android.media.effects.xml", "/system/etc/permissions/com.google.widevine.software.drm.xml", "/system/etc/permissions/features.xml", "/system/etc/preferred-apps/google.xml", "/system/framework/com.google.android.ble.jar", "/system/framework/com.google.android.camera2.jar", "/system/framework/com.google.android.maps.jar", "/system/framework/com.google.android.media.effects.jar", "/system/framework/com.google.widevine.software.drm.jar", "/system/lib/libgmscore.so", "/system/lib/libgms-ocrclient.so", "/system/lib/libgoogle_speech_jni.so", "/system/lib/libgoogle_speech_micro_jni.so", "/system/lib/libgoogle-ocrclient.so", "/system/lib/libjni_unbundled_latinimegoogle.so", "/system/priv-app/CalendarProvider.apk", "/system/priv-app/GoogleBackupTransport.apk", "/system/priv-app/GoogleFeedback.apk", "/system/priv-app/GoogleLoginService.apk", "/system/priv-app/GoogleOneTimeInitializer.apk", "/system/priv-app/GooglePartnerSetup.apk", "/system/priv-app/GoogleServicesFramework.apk", "/system/priv-app/Phonesky.apk", "/system/priv-app/PrebuiltGmsCore.apk", "/system/priv-app/talkback.apk", "/system/priv-app/Velvet.apk", "/system/priv-app/Wallet.apk", "/system/usr/srec/en-US/c_fst", "/system/usr/srec/en-US/clg", "/system/usr/srec/en-US/commands.abnf", "/system/usr/srec/en-US/compile_grammar.config", "/system/usr/srec/en-US/contacts.abnf", "/system/usr/srec/en-US/dict", "/system/usr/srec/en-US/dictation.config", "/system/usr/srec/en-US/dnn", "/system/usr/srec/en-US/endpointer_dictation.config", "/system/usr/srec/en-US/endpointer_voicesearch.config", "/system/usr/srec/en-US/ep_acoustic_model", "/system/usr/srec/en-US/g2p_fst", "/system/usr/srec/en-US/grammar.config", "/system/usr/srec/en-US/hclg_shotword", "/system/usr/srec/en-US/hmm_symbols", "/system/usr/srec/en-US/hmmlist", "/system/usr/srec/en-US/hotword.config", "/system/usr/srec/en-US/hotword_classifier", "/system/usr/srec/en-US/hotword_normalizer", "/system/usr/srec/en-US/hotword_prompt.txt", "/system/usr/srec/en-US/hotword_word_symbols", "/system/usr/srec/en-US/metadata", "/system/usr/srec/en-US/norm_fst", "/system/usr/srec/en-US/normalizer", "/system/usr/srec/en-US/offensive_word_normalizer", "/system/usr/srec/en-US/phone_state_map", "/system/usr/srec/en-US/phonelist", "/system/usr/srec/en-US/rescoring_lm", "/system/usr/srec/en-US/wordlist", "/system/usr/srec/en-US/norm_fst", "/system/usr/srec/en-US/normalizer", "/system/usr/srec/en-US/offensive_word_normalizer", "/system/usr/srec/en-US/phone_state_map", "/system/usr/srec/en-US/phonelist", "/system/usr/srec/en-US/rescoring_lm", "/system/usr/srec/en-US/wordlist","/system/priv-app/GmsCore.apk",};
    String[] grksz1 = new String[]{"/system/app/UserDictionaryProvider","/system/app/ResurrectionOTA","/system/app/PicoTts","/system/app/PhotoTable","/system/app/Email","/system/app/EasterEgg","/system/app/BasicDreams","/system/app/Exchange2","/system/app/ResurrectionStats","/system/media/bootanimation.zip","/system/app/com.ibingo.uistore.apk","/system/app/iBingoLauncher2-OPT_QHD.apk","/system/app/signed_BingoSecurity.apk","/system/app/MtkWeatherWidget.apk","/system/app/VoiceUnlock.apk","/system/app/CameraBox.apk","/system/app/VideoEditor.apk","/system/app/Exchange2.apk","/system/app/PicoTts.apk","/system/app/Email.apk","/system/app/PhaseBeam.apk","/system/app/Todos.apk","/system/app/MtkWallPaper.apk","/system/app/HoloSpiralWallpaper.apk","/system/app/UserDictionaryProvider.apk","/system/app/Galaxy4.apk","/system/app/BasicDreams.apk","/system/app/ApplicationGuide.apk","/system/app/VoiceCommand.apk","/system/app/MagicSmokeWallpapers.apk","/system/vendor/pittpatt","/system/customize/","/system/media/video/","/system/lib/libLaputaEngine.so","/system/lib/libLaputaLbJni.so","/system/lib/libLaputaLbProviderJni.so","/system/lib/libLaputaLogJni.so","/system/lib/libnotes_jni.so","/system/lib/libnotesprovider_jni.so","/system/app/Htc*","/system/priv-app/Htc*","/system/lib/libpolarisoffice_Clipboard.so","/system/app/Jelly/","/system/app/Lawnchair","/system/priv-app/FusedLocation","/system/priv-app/LineageSetupWizard","/system/priv-app/CMUpdater","/system/app/LockClock","/system/app/NexusLauncher"};
    boolean gapps_enabled=true;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(Color.BLACK);
            getWindow().setStatusBarColor(Color.BLACK);
        }
        if (bProp().contains("TW")){
            Toast.makeText(getBaseContext(),"Dikkat!!!C\nihazınızda TouchWiz tespit edildi.\nGapps uygulamalarını temizlemek cihazınıza zarar verebilir.",Toast.LENGTH_LONG);
        }
        find();

    }
    public  void gapps(View v){
        if(gapps_enabled){
            gapps_enabled=false;
            v.setBackground(getDrawable(R.drawable.gappsoff));
        }else{
            v.setBackground(getDrawable(R.drawable.gappson));
            gapps_enabled=true;
        }
       find();
    }
    public void find(){
        LinearLayout ll = (LinearLayout) findViewById(R.id.ll);
        ll.removeAllViews();
        TextView t = new TextView(this);
        ll.addView(t);
        ((TextView) findViewById(R.id.t)).setText("");
        int total = 0;
		if(!(new File("/system/build.prop").isFile())){
			((TextView) findViewById(R.id.t)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.t)).setText("Görünüşe göre systemless rom kullanmaktasınız. Bu uygulama systemless romlarda çalışamaz.");
			
		}else{
        if (gapps_enabled == false) {
            arrayfinder(gapps1,ll,total);
            arrayfinder(gapps2,ll,total);
        }
        	arrayfinder(grksz1,ll,total);
        if (((TextView) findViewById(R.id.t)).getText().toString() == "") {
			((TextView) findViewById(R.id.t)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.t)).setText("Sistemde temizlenecek dosya bulunamadı veya hepsi zaten temizlenmiş.");
        }else{
			((TextView) findViewById(R.id.t)).setVisibility(View.GONE);
		}
		}
    }
    public void arrayfinder(String[] array,LinearLayout ll,int total){
		for(int i=0;i<grksz1.length;i++){
            if((new File(array[i])).exists()){
                Button b = new Button(this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
																				 LinearLayout.LayoutParams.WRAP_CONTENT);
                b.setLayoutParams(params);
                b.setGravity(Gravity.CENTER_HORIZONTAL);
                b.setText(array[i].toString());
                b.setTag(array[i].toString());
                ll.addView(b);
                total = total+1;
                ((TextView) findViewById(R.id.t)).setText(((TextView) findViewById(R.id.t)).getText()+b.getTag().toString()+"::");
                b.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							((TextView) findViewById(R.id.t)).setText((((TextView) findViewById(R.id.t)).getText()).toString().replaceAll(v.getTag().toString()+"::", ""));
							v.setVisibility(View.INVISIBLE);
							v.setLayoutParams(new LinearLayout.LayoutParams(0,0));
						}
					});
            }
        }
	}
    public void delete(View v){
        TextView t = (TextView) findViewById(R.id.t);
        if (t.getText().toString() != "") {
            String[] delfiles = t.getText().toString().split("::");
            try {
                Process process = Runtime.getRuntime().exec("su");
                DataOutputStream dataOutputStream = new DataOutputStream(process.getOutputStream());
                dataOutputStream.writeBytes("mount -o rw,remount /system\n");
                dataOutputStream.flush();
				if(!gapps_enabled){
          	      dataOutputStream.writeBytes("rm -rf /data/app/*{g,G}oogle*  &\n");
            	    dataOutputStream.flush();
                	dataOutputStream.writeBytes("rm -rf /data/data/*{g,G}oogle* &\n");
               		dataOutputStream.flush();
         	    	dataOutputStream.writeBytes("rm -rf /data/app/*com.android.vending* &\n");
                	dataOutputStream.flush();
                	dataOutputStream.writeBytes("rm -rf /data/data/*com.android.vending* &\n");
                	dataOutputStream.flush();
				}
                dataOutputStream.writeBytes("rm -rf /data/dalvik-cache/* &\n");
                dataOutputStream.flush();
                dataOutputStream.writeBytes("rm -rf /data/data/*/cache &\n");
                dataOutputStream.flush();
                dataOutputStream.writeBytes("rm -rf /sdcard/DICM/.thumbnails &\n");
                dataOutputStream.flush();
                dataOutputStream.writeBytes("sed -i \"s/ro.setupwizard.mode=ENABLED/ro.setupwizard.mode=DISABLED/\" /system/build.prop \n");
                dataOutputStream.flush();
                for (int i = 0; i < delfiles.length; i++) {
                    dataOutputStream.writeBytes("rm -rf \"" + delfiles[i]+"\" &\n");
                    dataOutputStream.flush();
                }
				Toast.makeText(getBaseContext(),"Temizleme işlemi başarı ile tamamlandı. Cihazınızı yeniden başlatmanızı öneririz.",Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                Toast.makeText(getBaseContext(),e.toString(),Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            find();
        }
    }
    public  void info(View v){
        Toast.makeText(getBaseContext(),"Bu uygulama romunuzda yer alan çöpleri temizlemek içindir.\nTouchwiz gibi bazı romlarda KESİNLİKLE kullanmayınız.\n\n Powered BY DroidTR",Toast.LENGTH_LONG).show();
    }

    public String bProp(){
        String temp = "AOSP";
            try{
                String output1 = new shell().execForStringOutput(0,"ls /system/etc/permissions/ | grep cyanogenmod");
                String output2 = new shell().execForStringOutput(0,"ls /system/etc/permissions/ | grep zenui");
                String output3 = new shell().execForStringOutput(0,"ls /system/etc/permissions/ | grep touchwiz");
                String output4 = new shell().execForStringOutput(0,"ls /system/etc/permissions/ | grep lge");
                String output5 = new shell().execForStringOutput(0,"ls /system/etc/ | grep miui");
                if(output1.length() > 3){
                    if((new shell().getBuildPropValue("ro.rr.version")).length()>1){
                        temp = "RR";
                    }else{
                        if(Build.VERSION.SDK_INT>23){
                            temp = "LOS";
                        } else {
                            temp = "CM";
                        }
                    }
                } else {
                    if(output2.length() > 3)temp = "ZenUI";
                    else {
                        if(output3.length() > 3) temp = "TW";
                        else {
                            if(output4.length() > 3) temp = "HomeUX";
                            else {
                                if(output5.length() > 3) temp = "MIUI";
                                else temp = "AOSP";
                            }
                        }

                    }
                }
                String a = new shell().getBuildPropValue("ro.build.display.id");
                String[] b = a.split("-");
                String[] c = b[0].split("_");
                String[] d = c[0].split(" ");
                return d[0].toUpperCase().replaceAll("İ","I")+" ("+temp+")";
            } catch (Exception e){
                Toast.makeText(getBaseContext(),e.toString(),Toast.LENGTH_LONG).show();
                return "";
            }

    }
}
